<?php
/**
 * Template Name: Front Page
 */
?>

<?php get_header(); ?>

<?php
//
// Begin slider
//
if ( have_rows( 'homepage_slides' ) ): ?>

	<div id="homeCarousel" class="carousel slide" data-ride="carousel">

		<div class="carousel-inner" role="listbox">

			<?php
			$count = 0;

			// loop through the rows of slides
			while ( have_rows( 'homepage_slides' ) ) :
				the_row(); ?>

				<?php if ( $count == 0 ) { ?>

				<div class="home-slide carousel-item active" style="background: linear-gradient(rgba(0,0,0,.2),
					rgba(0,0,0,.2)), url(<?php echo the_sub_field( 'slider_image' ); ?>); background-size: cover; height: 100%;">


					<div class="slide-mini-images">
						<i class="fa fa-user-md wow fadeInUp animated" data-wow-delay="1.2s" aria-hidden=" true"></i>
						<i class="fa fa-medkit wow fadeInUp animated" data-wow-delay="1.6s" aria-hidden=" true"></i>
						<i class="fa fa-stethoscope wow fadeInUp animated" data-wow-delay="1.7s" aria-hidden="true"></i>
					</div>

					<div class="slide-copy">

						<h2 class="slide-title <?php echo the_sub_field( 'slide_title_animation' ) ?>"
						    data-wow-delay=".3s"
						><?php echo the_sub_field( 'slider_title' ); ?></h2>

						<h2 class="slide-subtitle <?php echo the_sub_field( 'slide_title_animation' ) ?>"
						    data-wow-delay=".3s"><?php echo the_sub_field( 'slide_sub_title' ); ?></h2>

						<p class="<?php echo the_sub_field( 'slide_copy_animation' ); ?>"
						   data-wow-delay=".3s"><?php echo the_sub_field( 'slider_copy' ); ?></p>

						<a
							class="<?php echo the_sub_field( 'slide_button_animation' ); ?> btn btn-primary"

							href="<?php the_sub_field( 'slider_link' ); ?>" data-wow-delay=".3s">

							<?php echo the_sub_field( 'slider_button' ); ?>

						</a>

					</div>

				</div>

			<?php } else { ?>

				<div class="home-slide carousel-item" style="background: linear-gradient(rgba(0,0,0,.2),
					rgba(0,0,0,.2)), url(<?php echo the_sub_field( 'slider_image' ); ?>); background-size: cover; height: 100%">

					<div class="slide-copy">

						<h2 class="slide-title <?php echo the_sub_field( 'slide_title_animation' ) ?>"
						    data-wow-delay=".5s"
						><?php echo the_sub_field( 'slider_title' ); ?></h2>

						<h2 class="slide-subtitle <?php echo the_sub_field( 'slide_title_animation' ) ?>"
						    data-wow-delay=".5s"><?php echo the_sub_field( 'slide_sub_title' ); ?></h2>

						<p class="<?php echo the_sub_field( 'slide_copy_animation' ); ?>"
						   data-wow-delay="1s"><?php echo the_sub_field( 'slider_copy' ); ?></p>

						<a class="btn btn-primary"
							href="<?php the_sub_field( 'slider_link' ); ?>" data-wow-delay="1.5s">

							<?php echo the_sub_field( 'slider_button' ); ?>

						</a>

					</div>

				</div>


			<?php } ?>

				<?php
				$count ++;
			endwhile; ?>

			<a class="carousel-control-prev" href="#homeCarousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#homeCarousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>


		</div>


	</div>

	<?php
else :

	// no rows found

endif; ?>

<?php
//
// End slider
//
?>

<?php

//
//Begin Call to Action
//

// check if the call to action field has rows of data
if ( have_rows( 'call_to_action_boxes' ) ): ?>

	<div class="container"> <!--call to action container-->

		<div class="row cta-row"> <!--call to action row-->

			<?php

			// loop through the rows of data
			while ( have_rows( 'call_to_action_boxes' ) ) :
				the_row(); ?>

				<div class="cta-item col-md-12 col-lg-4">

					<div class="cta-copy <?php echo the_sub_field( 'animation' ) ?>"
					     data-wow-duration="<?php echo the_sub_field( 'animation_duration' ) ?>">

						<i class="<?php the_sub_field( 'icon' ); ?>" aria-hidden="true"></i>

						<h2><?php the_sub_field( 'cta_title' ); ?></h2>

						<p><?php the_sub_field( 'cta_description' ); ?></p>

						<?php $ctaLink = get_sub_field( 'cta_link' ); ?>

						<?php if ( $ctaLink ) { ?>

							<a class="btn btn-primary slide-effect"

							   href="<?php the_sub_field( 'cta_link' ); ?>">

								Learn More

							</a>

						<?php } ?>

					</div>

				</div>

				<?php

			endwhile;

			else :

				// no rows found

			endif; ?>

		</div> <!--end cta row-->

	</div> <!--end cta container-->

<?php
//
//End Call to Action
//

?>

<?php
//
//Begin Welcome Message
//

?>

	<div class="container-fluid"> <!--welcome message container-->

		<div class="welcome row">

			<div class="welcome-intro wow fadeInLeft col-xs-12 col-lg-6 offset-lg-1">

				<h2><?php the_field( 'welcome_title' ); ?></h2>

				<h2><span><?php the_field( 'welcome_sub' ) ?></span></h2>

				<p><?php the_field( 'welcome_copy' ); ?></p>

			</div>

			<div class="welcome-image-container wow fadeInRight hidden-md-down col-xs-12 col-lg-5">

				<div class="welcome-image"
				     style="background-image: url('<?php the_field( 'welcome_image' ); ?>'); background-size: cover; padding: 0px;">

				</div>

			</div>


		</div>

	</div>

<?php
//
//End Welcome message
//

?>

<?php
//
//Begin Departments
//
?>

<?php

// check if the departments  has rows of data
if ( have_rows( 'departments_specialties' ) ): ?>
	<section style="background: linear-gradient(rgba(255,255,255,.4),
		rgba(255,255,255,.9)),url(<?php echo the_field( 'department_specialties_image' ); ?>); background-position: center center; background-size: cover;">

		<div class="departments container wow zoomIn" data-wow-duration="1s"

		<!--main container-->

		<div class="title-container">
			<div class="dots text-center">
                    <span>
                      <span></span>
                      <span></span>
                      <span></span>
                    </span>
			</div>
			<h2 class="text-center text-uppercase">Our <span>Departments</span></h2>
			<p class="text-center">ACENTA offers a comprehensive treatments for a variety of conditions. Listed below
				are all of the departments, click on them to learn more.</p>
		</div>
		<div class="row">

			<?php

			// loop through the rows of data
			while ( have_rows( 'departments_specialties' ) ) : the_row(); ?>

				<div class="col-sm-4 text-center">
					<a href="<?php the_sub_field( 'department_link' ); ?>">

						<div class="department-item ">
							<i class="<?php the_sub_field( 'department_icon' ); ?>"></i>
							<h4 class="text-uppercase font-weight-bold"><?php the_sub_field( 'department_title' ); ?></h4>
							<div class="divider"></div>
							<p><?php the_sub_field( 'department_copy' ); ?></p>

						</div>
					</a>
				</div>

			<?php endwhile;

			else :

				// no rows found

			endif; ?>

		</div> <!--end row-->

		</div> <!--end main department container-->

	</section>
<?php
//
//End Departments
//
?>


<?php
//
//Begin Testimonials/FAQ
//
?>
	<div class="container-fluid"><!--begin testimonials-->

		<div class="faq-testimonial row wow fadeIn" data-wow-duration="2s">

			<div class="testimonials col-xs-12 col-md-5 col-lg-5"
			     style="  background: linear-gradient(rgba(0, 0, 0, .6),
				     rgba(0, 0, 0, .6)), url(<?php echo the_field( 'testimonial_image' ); ?>);
				     background-size: cover;">

				<?php

				// check if the repeater field has rows of data
				if ( have_rows( 'testimonials' ) ): ?>

				<a class="left" href="#carousel-testimonials" role="button"
				   data-slide="prev">
					<span class="icon-prev" aria-hidden="true"></span>
					<span class="sr-only"></span>
				</a>
				<a class="right" href="#carousel-testimonials" role="button"
				   data-slide="next">
					<span class="icon-next" aria-hidden="true"></span>
					<span class="sr-only"></span>
				</a>

				<h2 class="text-uppercase">Reviews</h2>

				<div id="carousel-testimonials" class="carousel slide" data-ride="carousel">

					<div class="carousel-inner" role="listbox">
						<?php
						// loop through the rows of data
						while ( have_rows( 'testimonials' ) ) : the_row(); ?>

							<?php if ( $testimonialCount == 1 ) { ?>

								<div class="carousel-item active">

									<p><?php the_sub_field( 'testimonial' ); ?></p>

									<h3><?php the_sub_field( 'testimonial_name' ); ?></h3>

								</div>

							<?php } elseif ( $testimonialCount != 1 ) { ?>

								<div class="carousel-item">

									<p><?php the_sub_field( 'testimonial' ); ?></p>

									<h3><?php the_sub_field( 'testimonial_name' ); ?></h3>

								</div>


							<?php } ?>

							<?php $testimonialCount ++; ?>


						<?php endwhile;

						else :

							// no rows found

						endif;

						?>
					</div>

				</div> <!--end carousel-->

			</div> <!--end testimonials-->

			<div class="faqs col-xs-12 col-md-7 col-lg-7" id="accordion" role="tablist" aria-multiselectable="true">

				<?php $faqCount = 1; ?>

				<?php
				// check if the repeater field has rows of data
				if ( have_rows( 'faqs' ) ): ?>


					<h2 class="text-uppercase">Faqs</h2>

					<?php // loop through the rows of data
					while ( have_rows( 'faqs' ) ) : the_row(); ?>

						<?php if ( $faqCount == 1 ) { ?>

							<div class="faq-item">

								<h3 class="mb-0">
									<a class="active" data-toggle="collapse" data-parent="#accordion"
									   href="#collapse<?php echo $faqCount; ?>"
									   aria-expanded="true"
									   aria-controls="collapse<?php echo $faqCount; ?>">
										<i class="fa fa-minus" aria-hidden="true"></i>
										<?php the_sub_field( 'question' ); ?>
									</a>
								</h3>

								<div id="collapse<?php echo $faqCount; ?>" class="collapse show" role="tabpanel">
									<p><?php the_sub_field( 'answer' ); ?></p>
								</div>

							</div>

						<?php } elseif ( $faqCount != 1 ) { ?>
							<div class="faq-item">

								<h3 class="mb-0">
									<a data-toggle="collapse" data-parent="#accordion"
									   href="#collapse<?php echo $faqCount; ?>"
									   aria-expanded="false"
									   aria-controls="collapse<?php echo $faqCount; ?>">
										<i class="fa fa-plus" aria-hidden="true"></i>
										<?php the_sub_field( 'question' ); ?>
									</a>
								</h3>

								<div id="collapse<?php echo $faqCount; ?>" class="collapse" role="tabpanel">
									<p><?php the_sub_field( 'answer' ); ?></p>
								</div>

							</div>

						<?php } ?>

						<?php $faqCount ++; ?>

					<?php endwhile;

				else :

					// no rows found

				endif;

				?>
			</div> <!--end faqs-->

		</div>

	</div> <!--end container-->


<?php
//
//End Testimonials/FAQ
//
?>


<?php
//
//Begin Staff
//
?>

	<div class="container wow fadeIn" data-wow-duration="2s">

		<div class="staff-intro col-xs-12 text-center">

			<div class="dots">
                    <span>
                      <span></span>
                      <span></span>
                      <span></span>
                    </span>
			</div>

			<h2 class="text-uppercase">Our <span>Team</span></h2>

		</div>

		<div class="staff row"> <!--meet the staff-->

			<?php

			/*grab all staff from staff post type*/
			$post_objects = get_field( 'staff_members' );

			if ( $post_objects ): ?>


				<!--loop through staff-->
				<?php foreach ( $post_objects as $post_object ): ?>

					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_object->ID ), 'single-post-thumbnail' ); ?>

					<div class="col-xs-12 col-md-4">

						<div class="staff-member text-center">

							<img src="<?php echo $image[0]; ?>">

							<h4 class="text-uppercase"><?php echo get_the_title( $post_object->ID ); ?></h4>

							<p class="position"><?php the_field( 'position', $post_object->ID ); ?></p>

							<div class="divider"></div>

							<p><?php the_field( 'excerpt', $post_object->ID ); ?></p>

						</div>

					</div>

				<?php endforeach; ?>

			<?php endif;

			?>

		</div> <!--end row-->

	</div> <!--end staff container-->


	<script type="text/javascript">

		(function ($) {
			$('.carousel').carousel({
				interval: false
			});

		})(jQuery);

	</script>


<?php get_footer(); ?>