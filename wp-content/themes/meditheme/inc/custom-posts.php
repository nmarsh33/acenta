<?php
/**
 * Custom Post Types
 */

/*
 *Staff
 *
 * */
add_action( 'init', 'staff_custom' );
function staff_custom() {
	register_post_type( 'staff',
		array(
			'labels'      => array(
				'name'              => __( 'Staff' ),
				'singular_name'     => __( 'Staff' ),
				'add_new'           => _x( 'Add New', 'Staff Member' ),
				'add_new_item'      => __( 'Add New Staff Member' ),
				'edit_item'         => __( 'Edit Staff Member' ),
				'new_item'          => __( 'New Staff Member' ),
				'all_items'         => __( 'All Staff Members' ),
				'view_item'         => __( 'View Staff Member' ),
				'parent_item_colon' => '',
				'menu_name'         => __( 'Staff' )
			),
			'public'      => true,
			'has_archive' => true,
			'rewrite'     => array( 'slug' => 'staff' ),
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
			'menu_icon'           => 'dashicons-groups',
		)
	);
}

/*
 * General Options
 *
 * */