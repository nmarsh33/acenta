<?php
/**
 *
 * Styles: Frontend with no conditions, Add Custom styles to wp_head
 *
 * @since  1.0
 *
 */
add_action( 'wp_enqueue_scripts', 'medi_styles' ); // Add Theme Stylesheet
function medi_styles() {

	/**
	 *
	 * Minified and Concatenated styles
	 *
	 */
	wp_register_style( 'medi_style', get_template_directory_uri() . '/style.min.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'medi_style' ); // Enqueue it!

	/*Font Awesome*/
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );
	                                                                                                                                                                                                            /*Google Font*/
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:100,400,700,900', false );
	wp_enqueue_style( 'wpb-google-fonts2', 'https://fonts.googleapis.com/css?family=Montserrat:400,700', false );

}

/**
 *
 * Scripts: Frontend with no conditions, Add Custom Scripts to wp_head
 *
 * @since  1.0.0
 *
 */
add_action( 'wp_enqueue_scripts', 'medi_scripts' );
function medi_scripts() {
	if ( $GLOBALS['pagenow'] != 'wp-login.php' && ! is_admin() ) {

		wp_enqueue_script( 'jquery' ); // Enqueue it!

		/**
		 *
		 * Minified and concatenated scripts
		 *
		 * @vendors     plugins.min,js
		 * @custom      scripts.min.js
		 *
		 *     Order is important
		 *
		 */
		wp_register_script( 'medi_customJS', get_template_directory_uri() . '/assets/js/custom.min.js#asyncload' ); // Custom scripts
		wp_enqueue_script( 'medi_customJS' ); // Enqueue it!

		wp_register_script( 'medi_bootstrapJS', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js#asyncload' ); // Custom scripts
		wp_enqueue_script( 'medi_bootstrapJS' ); // Enqueue it!


		wp_register_script( 'dropdown_menuJS', get_template_directory_uri() . '/assets/js/bootstrap-hover-dropdown.min.js#asyncload' ); // Custom scripts
		wp_enqueue_script( 'dropdown_menuJS' ); // Enqueue it!

	}

}