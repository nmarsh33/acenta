<?php
/**
 * Manages the Hero Section on all pages
 *
 * @author Nick Marsh
 * @return
 */

function hero() {

	$pageID       = get_option( 'page_for_posts' );
	$heroImage    = get_field( 'hero_image', $pageID );
	$heroSubTitle = get_field( 'hero_sub_title' );

	if ( $heroImage ) {

		?>
		<!--Hero-->
		<section class="hero" style="background: linear-gradient(
			rgba(16, 128, 208, 0.5),
			rgba(16, 128, 208, 0.5)
			), url('<?php echo $heroImage; ?>'); background-position: center center; background-size: cover;">
			<div class="content-container col-lg-12">
				<h2 class="text-uppercase animate animated fadeIn"><?php the_title(); ?></h2>
				<!--			<h4 class="animated fadeIn"><?php /*echo $heroSubTitle; */ ?></h4>
-->        </div>
		</section>

	<?php } ?>

<?php } ?>
