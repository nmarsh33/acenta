<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title"
	      content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
	      integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<?php wp_head(); ?>
	
	<!--Google Analytics-->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-96364137-1', 'auto');
		ga('send', 'pageview');

	</script>
</head>

<body <?php body_class(); ?>>

<div class="top-contact container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<p><i class="fa fa-mobile" aria-hidden="true"></i><span>Call Us:</span> <a href="tel:4792424220">479-242-4220</a></p>
		</div>
	</div>
</div>

<div class="header clearfix">

	<!--<nav class="navbar navbar-toggleable-md navbar-light">
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
		        data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
		        aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>-->
		<a class="navbar-brand" href="/">
			<img src="<?php echo the_field( 'logo', 'option' ); ?>">
		</a>

		<!--<div class="collapse navbar-collapse" id="navbarNavDropdown">-->
			<!-- The WordPress Menu goes here -->
			<?php wp_nav_menu(
				array(
					'theme_location' => 'primary',
					'menu_class'     => 'nav navbar-nav navbar',
					'fallback_cb'    => '',
					'menu_id'        => 'main-menu',
					'walker'         => new wp_bootstrap_navwalker()
				)
			); ?>
	<!--	</div>
	</nav>
-->

</div>

